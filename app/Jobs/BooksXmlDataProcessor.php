<?php

namespace App\Jobs;

use App\Interfaces\BookInterface;
use \Gumlet\ImageResize;

class BooksXmlDataProcessor extends Job
{

    protected $xml;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($xml)
    {
        $this->xml = $xml;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(BookInterface $book)
    {
        $xml = simplexml_load_string($this->xml);
        $jsonStringified = json_encode($xml);
        $jsonParsed = json_decode($jsonStringified, TRUE);
        $books = $jsonParsed['books']['book'];

        foreach ($books as $bookData) {
            $bookAlreadyExists = $book->getBookByIsbn($bookData['@attributes']['isbn']);

            if (empty($bookAlreadyExists)) {
                $data = array(
                    'title' => $bookData['@attributes']['title'],
                    'isbn' => $bookData['@attributes']['isbn'],
                    'description' => $bookData['description'],
                    'image' => null,
                    'image' => array_key_exists('image', $bookData) ? $this->processImage($bookData['image']) : null
                );

                $book->createBook($data);
            }
        }
    }

    public function processImage($imageUrl)
    {
        $image = file_get_contents($imageUrl);

        $uniqueId = $this->gen_uuid();
        $year = date("Y");
        $month = date("m");
        $day = date("d");
        $imagePath = "$year/$month/$day/$uniqueId.jpg";

        $dirPrefix = "public/storage";

        if (!is_dir($dirPrefix)) {
            mkdir($dirPrefix);
        }

        if (!is_dir("$dirPrefix/$year")) {
            mkdir("$dirPrefix/$year");

            if (!is_dir("$dirPrefix/$year/$month")) {
                mkdir("$dirPrefix/$year/$month");

                if (!is_dir("$dirPrefix/$year/$month/$day")) {
                    mkdir("$dirPrefix/$year/$month/$day");
                }
            }
        }

        file_put_contents("$dirPrefix/$imagePath", $image);
        $image = new ImageResize("$dirPrefix/$imagePath");
        $image->crop(200, 400);
        $image->save("$dirPrefix/$imagePath");

        return $imagePath;
    }

    function gen_uuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0x0fff) | 0x4000,
            mt_rand(0, 0x3fff) | 0x8000,
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)
        );
    }
}
