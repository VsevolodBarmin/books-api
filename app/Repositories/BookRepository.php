<?php

namespace App\Repositories;

use App\Interfaces\BookInterface;
use App\Models\Book;

class BookRepository implements BookInterface
{

    private $book;

    public function __construct(Book $book)
    {
        $this->book = $book;
    }

    public function getBooks($page, $limitPerPage, $search)
    {
        $books = $this->book->where('isbn', 'LIKE', '%' . $search . '%')
            ->orWhere('title', 'LIKE', '%' . $search . '%')->paginate($limitPerPage, ['*'], 'page', $page);

        return $books;
    }

    public function getBookByIsbn($isbn)
    {
        return $this->book->where(array('isbn' => $isbn))->first();
    }

    public function createBook($data)
    {
        return $this->book->create($data);
    }

    public function updateBook($data)
    {
        return $this->book->update($data);
    }

    public function removeBookById($bookId)
    {
        $book = $this->book->find($bookId);

        if (empty($book)) {
            return false;
        }

        $book->delete();

        return true;
    }
};
