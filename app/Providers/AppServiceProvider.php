<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $models = array(
            'Book'
        );

        foreach ($models as $model) {
            $this->app->bind("App\Interfaces\\{$model}Interface", "App\Repositories\\{$model}Repository");
        }
    }
}
