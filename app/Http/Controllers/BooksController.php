<?php

namespace App\Http\Controllers;

use App\Interfaces\BookInterface;
use App\Jobs\BooksXmlDataProcessor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Queue;

class BooksController extends Controller
{
    private $book;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(BookInterface $bookInterface)
    {
        $this->book = $bookInterface;
    }

    public function uploadBooksXml(Request $request)
    {
        $this->validate($request, array(
            'xml' => 'required|file|max:262146'
        ));

        $xml = $request->file('xml');

        $content = file_get_contents($xml->getRealPath());
        $job = new BooksXmlDataProcessor($content, $this->book);

        Queue::push($job);

        $responseData = array("message" => "successfully uploaded xml.");

        return response()->json($responseData);
    }

    public function getBooks(Request $request)
    {
        $validatedData = $this->validate($request, array(
            'page' => ['required', 'numeric'],
            'limit' => ['required', 'numeric'],
            'search' => ['string']
        ));

        $page = $validatedData['page'];
        $limit = $validatedData['limit'];
        $search = $validatedData['search'];

        $books = $this->book->getBooks($page, $limit, $search);

        $responseData = array(
            'books' => $books
        );

        return response()->json($responseData);
    }
}
