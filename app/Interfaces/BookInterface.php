<?php

namespace App\Interfaces;

interface BookInterface {
    
    public function getBooks($page, $limitPerPage, $search);

    public function getBookByIsbn($isbn);

    public function createBook($data);

    public function updateBook($data);

    public function removeBookById($id);
}